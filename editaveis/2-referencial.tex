\chapter{Referencial Teórico}

\section{A Nova Administração Pública e sua Evolução}

Para uma definição inicial, \citeonline{paludo2013} diz que Estado refere-se à convivência humana, à sociedade política, e detém o significado de poder, força, direito. Já \citeonline{kohama2003}, resume Estado como a Nação politicamente organizada. Dessa forma, pode-se concluir, num primeiro momento, que Estado é a organização política de uma sociedade exercida pelo poder do soberano.

De uma forma menos simplista, \citeonline{moraes2010} conceitua Estado como a forma histórica de organização jurídica, limitado a um determinado território, com população definida e dotado de soberania, que, em termos gerais e no sentido moderno, configura-se como um poder supremo no plano interno e um poder independente no plano internacional.

\citeonline{paludo2013} vai além e fragmenta a compreensão do conceito de Estado:

\begin{itemize}

\item No sentido lato – Estado é a nação politicamente organizada; é quem detém o poder soberano: independência externa e soberania interna.
\item No sentido jurídico – Estado é a pessoa jurídica de Direito Público Interno responsável pelos atos de seus agentes ou pessoa jurídica de Direito Público Internacional no trato com os demais países.
\item No sentido social – Estado é um agrupamento de pessoas que residem num determinado território e se sujeitam ao poder soberano, em que apenas alguns exercem o poder.
\item No sentido político/administrativo – Estado é o exercício efetivo do poder através do Governo/administração, em prol do bem comum.

\end{itemize}

A organização do Estado brasileiro é assunto tratado no Direito Constitucional, no que se refere à sua divisão político-territorial, forma de governo, estrutura dos poderes, ao modo de aquisição e exercício do poder e aos direitos e garantias individuais e sociais dos governados \cite{paludo2013}.			

Entrando no conceito de Administração Pública, de acordo com \citeonline{pietro2012}, pode ser entendida como ``a atividade que o Estado exerce em prol do atendimento das necessidades da sociedade (objetivo ou material), ou o conjunto de entidades que a compõem no exercício da função administrativa (subjetivo ou formal)''.
 
De acordo com \citeonline{paludo2013}, o termo administração é utilizado tanto para designar funções de planejamento e direção, como para designar as atividades de execução. O administrador, por sua vez, é o profissional (agente público) que atua nas organizações e exerce as funções de planejar, organizar, dirigir, coordenar e controlar. A alta administração corresponde aos agentes com poder de decisão, que deverão tomar as decisões certas e a tempo, a fim de conduzir a organização ao alcance dos objetivos institucionais, ao crescimento e à sustentabilidade. Em resumo, a administração compreende todo o aparato existente à disposição dos governos para a realização dos seus objetivos políticos e do objetivo maior e primordial do Estado: a promoção do bem comum da coletividade.

No Brasil, o principal marco para o surgimento da Administração Pública ocorreu em 1808 com a chegada da família real portuguesa, período em que o país foi elevado à parte integrante do Reino de Portugal. Dentro de uma perspectiva histórico-evolutiva, é possível distinguir três modelos diferentes de Administração Pública: o patrimonialista, o burocrático e o gerencial.


\subsection{A Nova Administração Pública: Modelos e Evolução} 

\subsubsection{Modelo Patrimonialista}

O modelo patrimonialista, originário das monarquias absolutistas europeias, foi o primeiro modelo adotado no Brasil. No patrimonialismo, o aparelho do Estado funciona como uma extensão do poder do soberano, e os seus auxiliares, servidores, possuem status de nobreza real. Os cargos são considerados prebendas \cite{marques2008}.	 

\citeonline{paludo2013} afirma que nesse período histórico, o Estado-Administração não pensava de forma coletiva e não procurava prestar serviços à população, que era relegada ao descaso. Consequentemente, o foco das ações não era o atendimento das necessidades sociais e nem o desenvolvimento da nação, e os benefícios oriundos do Estado e da Administração não eram destinados ao povo, mas para um pequeno grupo encabeçado pelo chefe do Executivo (o soberano).

Portanto, pode-se inferir que o patrimonialismo tem como características o poder ilimitado do governante, ausência de distinção entre bens públicos e particulares e nepotismo, além de ausência de participação popular no governo. Em decorrência dessas características, a corrupção é inerente a esse modelo.

Essa forma de administração patrimonialista vigorou nos Estados, de forma predominante, até a segunda metade do século XIX, quando o surgimento de organizações de grande porte, o processo de industrialização e as demandas sociais emergentes forçaram os governos a adotar um novo modelo de administração capaz de responder tanto aos anseios dos comerciantes e industriais, quanto aos da sociedade em geral. Em países como o Brasil, o Estado-Administração ainda teria a missão de alavancar o processo de desenvolvimento nacional \cite{paludo2013}.

\subsubsection{Modelo Burocrático}

O modelo burocrático, idealizado pelo sociólogo alemão Max Weber, tem sua origem na ética protestante e no liberalismo.	Segundo o modelo weberiano, a administração burocrática seria uma “forma de dominação”, possuindo uma racionalidade oriunda da cultura protestante, que teria causado uma influência decisiva na evolução da sociedade moderna.

Conforme \citeonline{paludo2013}, em face da desorganização do Estado em termos de prestação de serviços públicos e da ausência de um projeto de desenvolvimento para a nação, aliadas à corrupção e ao nepotismo comuns na área pública, um novo modelo de administração se fazia necessário. Era preciso reestruturar e fortalecer a Administração Pública para que pudesse cumprir suas novas funções. O surgimento das organizações de grande porte, a pressão pelo atendimento de demandas sociais, o crescimento da burguesia comercial e industrial indicavam que o Estado liberal deveria ceder seu espaço a um Estado mais organizado e de cunho econômico.

No Brasil, o modelo burocrático emerge a partir dos anos 30, em meio ao desenvolvimento da indústria brasileira e surge como forma de combater a corrupção e o nepotismo patrimonialista.

A classe dirigente é formada pela aliança entre a burguesia industrial e a burocracia pública. Nesse período, o país experimenta um grande desenvolvimento econômico. O patrimonialismo, entretanto, ainda que sofresse um processo de transformação, mantinha sua própria força no quadro político brasileiro \cite{marques2008}.

Esse modelo trouxe como princípios a profissionalização, hierarquia funcional, impessoalidade, formalismo, em síntese, o poder racional-legal. Por outro lado,  conforme \citeonline{paludo2013}:

\begin{quotation}

``[...] o controle – a garantia do poder do Estado – transforma-se na própria razão de ser do funcionário. Em consequência, o Estado volta-se para si mesmo, perdendo a noção de sua missão básica, que é servir à sociedade. A qualidade fundamental da Administração Pública burocrática é a efetividade no controle dos abusos; seu defeito, a ineficiência, a autorreferência e a incapacidade de voltar-se para o serviço aos cidadãos.
Como o modelo anterior, a burocracia sucumbiu ao cenário dinâmico de sociedade. Porém, não foi extinto pelo modelo sucessor.''

\end{quotation}


\subsubsection{Modelo Gerencial}

De acordo com o “Plano Diretor da Reforma do Aparelho do Estado” \cite{pdrae1995}, a Administração Pública Gerencial emerge na segunda metade do século XX, como resposta, de um lado, à expansão das funções econômicas e sociais do Estado e, de outro, ao desenvolvimento tecnológico e à globalização da economia mundial, uma vez que ambos deixaram à mostra os problemas associados à adoção do modelo anterior.

A administração gerencial constitui um avanço, pois, embora não represente  um rompimento absoluto com o modelo anterior, passa a ter a forma de controle baseada nos resultados, com ênfase na eficiência e efetividade.

Esse modelo passou por um processo evolutivo e, ainda hoje, encontra-se em desenvolvimento a fim de melhor adapta-se às demandas internas e externas. O modelo gerencial, apesar de ser relativamente recente na história da administração pública brasileira, é considerado um avanço, na medida em que sua implantação representa não apenas a mudança de um modelo de administrar, mas uma mudança cultural pautada no aumento da qualidade e da eficiência na prestação de serviços sociais oferecidos pelo setor público.

Embora, historicamente, seja marcante um tipo predominante de administração, é possível afirmar que, na atualidade, a administração gerencial é o modelo vigente; que a administração burocrática ainda é aplicada no núcleo estratégico do Estado e em muitas organizações públicas; e que persistem traços/práticas patrimonialistas de administração nos dias atuais. É possível afirmar, ainda, que existem fragmentos de todas as teorias administrativas nas organizações públicas. O Governo Federal tem envidado esforços para a implantação do modelo gerencial de Administração Pública no Brasil, no entanto, práticas patrimonialistas ainda são vistas na Administração Pública Federal, Estadual e Municipal, e não somente no Poder Executivo, mas também no Legislativo e no Judiciário \cite{paludo2013}.

De acordo com \citeonline{torres2004}, a verdade é que nem mesmo o modelo burocrático foi plenamente implantado no Estado brasileiro, que permanece sendo administrado através de práticas que desconhecem ou ignoram os princípios da impessoalidade, publicidade, especialização, profissionalismo etc.
Segundo \citeonline{costa2008}:

\begin{quotation}

``A história da administração pública brasileira pode ser caracterizada por movimentos de centralização e descentralização, pelo convívio de modelos burocráticos incompletos e modelos gerenciais, portanto híbridos, fruto, tudo indica, de reformas que padeceram da falta de planejamento e de meios eficazes que dessem conta de sua implementação.''

\end{quotation}

%==============================================================================

\section{A Reconstrução do Aparelho do Estado no Brasil}

Esse capítulo se inicia com a diferenciação entre Estado e aparelho do Estado. Sendo  assim, de acordo com o que define o \citeonline{pdrae1995}, entende-se por aparelho do Estado a administração pública em sentido amplo, ou seja, a estrutura organizacional do Estado, em seus três poderes (Executivo, Legislativo e Judiciário) e três níveis	(União, Estados-membros e Municípios). O aparelho do Estado é constituído pelo governo, ou seja, pela cúpula dirigente dos Três Poderes, por um corpo de funcionários, e pela força militar. Já o Estado é mais abrangente que o aparelho porque compreende adicionalmente o sistema constitucional-legal, que regula a população nos limites de um território. 	 

Estes conceitos ajudam distinguir a Reforma do Estado, projeto amplo que diz respeito às várias áreas do governo e ao conjunto da sociedade como um todo, da reforma do aparelho do Estado, cujo escopo é mais restrito, orientado para a cidadania e voltado para tornar a administração pública mais eficiente.

Para este estudo, o foco será na reforma do aparelho do Estado, que, no entanto, não pode ser concebida fora da perspectiva de redefinição do papel do Estado e, portanto, pressupõe o reconhecimento prévio das modificações observadas em suas atribuições ao longo do tempo, com a evolução dos três modelos de administração pública: patrimonialista, burocrático e gerencial. Esses modelos intercorrem sucessivamente no tempo, sem que, no entanto, se eliminem completamente. 

\subsection{A Reforma Proposta no PDRAE}

O Plano Diretor de Reforma do Aparelho do Estado, o \citeonline{pdrae1995}, apresentou um diagnóstico da administração pública brasileira à época, no qual estava focada a atenção, de um lado, nas condições do mercado de trabalho e na política de recursos humanos, e, de outro, na distinção de três dimensões dos problemas:

\begin{enumerate}

\item a dimensão institucional-legal, relacionada aos obstáculos de ordem legal para o alcance de uma maior eficiência do aparelho do Estado;
\item a dimensão cultural, definida pela coexistência de valores patrimonialistas e principalmente burocráticos com os novos valores gerenciais e modernos na administração pública brasileira;
\item a dimensão gerencial, associada às práticas administrativas. As três dimensões estão inter-relacionadas. Apesar das dificuldades, acreditava-se ser possível promover de imediato a mudança da cultura administrativa e reformar a dimensão-gestão do Estado, enquanto ia sendo providenciada a mudança do sistema legal.

\end{enumerate}

O diagnóstico mostrava que para uma reforma consistente do aparelho do Estado necessitava-se mais que um mero rearranjo de estruturas. A superação das formas tradicionais de ação estatal implicavam descentralizar e redesenhar estruturas, dotando-as de inteligência e
flexibilidade, e sobretudo desenvolver modelos gerenciais para o setor público capazes de gerar resultados. 

A modernização do aparelho do Estado exigia, também, a criação de mecanismos que viabilizassem a integração dos cidadãos no processo de definição, implementação e avaliação da ação pública. Através do controle social crescente seria possível garantir serviços de qualidade e o PDRAE foi criado para estabelecer as diretrizes para uma ampla reforma administrativa.

Com a publicação do \citeonline{pdrae1995}, o Aparelho do Estado foi dividido em quatro núcleos:

\begin{enumerate}

\item Núcleo estratégico, composto pelos órgãos independentes e Ministros de Estado;
\item Núcleo de atividades exclusivas, composto por órgãos de entidades que desenvolvem atividades típicas do Estado, como por exemplo as Agências Reguladoras;
\item Núcleo de atividades não exclusivas, relacionado a atividades que podem ser realizadas pelo Estado, setor privado e setor público não estatal (terceiro setor) e ao estado de bem estar social (saúde, educação, cultura, esporte, pesquisa etc.);
\item Núcleo de produção para o mercado, trata-se do intervencionismo estatal no campo econômico por meio da produção direta de bens e serviços. Relaciona-se à empresas públicas.

\end{enumerate}

Em resumo, a reforma do aparelho do Estado significou melhorar não apenas a organização e o pessoal do Estado, mas também as finanças e todo o sistema institucional-legal, a fim de se estabelecer uma relação harmoniosa  e positiva com a sociedade civil. Porém, houve também uma forte reação à proposta liberal de privatização das empresas estatais ocorrida no início dos anos 1990, provocando tensão entre os interlocutores, e também críticas a pouca experiência na implantação de reformas desta ordem  em regimes democráticos em nosso país \cite{abrucio2007}.

As reformas ainda hoje esbarram em grandes obstáculos internos. Um deles é falta de cooperação entre a vertente do controle e a vertente gerencial. Além disso, o patrimonialismo e o clientelismo presentes na cultura política brasileira, também são perceptíveis na administração pública. 


\subsection{Programa Nacional de Gestão Pública e Desburocratização - Gespública}

Hoje, conforme \citeonline{barcelos2013}, dentro da Administração existem Escolas que estudam a Administração Pública especificamente, e nesses estudos aparecem teorias novas como a Administração Gerencial, Accountability e Nova Gestão Pública. Conceitos que trazem para a gestão pública assuntos da iniciativa privada como transparência, eficiência, produtividade e responsabilização. Dentro desses conceitos podem ser encontradas as respostas para as dúvidas quanto ao melhor formato de desempenho do Governo. 

Seguindo essa linha de melhoria da qualidade dos serviços públicos prestados aos cidadãos e o aumento da competitividade do país, em 2005 foi instituído o Decreto 5.378/05 que trata do Programa Nacional de Gestão Pública e Desburocratização, o Gespública.

O Gespública foi criado com a finalidade de alcançar integração entre os órgãos da administração pública, alcançar os resultados preconizados pelo plano plurianual (PPA), consolidar uma administração profissional e adotar técnicas gerenciais. Para isso, tem como objetivos básicos:

\begin{enumerate}

\item eliminar o deficit institucional;
\item promover a governança;
\item promover a eficiência;
\item garantir a eficácia e efetividade e;
\item promover uma gestão aberta, participativa e democrática.

\end{enumerate}

Nesse contexto, existe um desejado maior equilíbrio entre a atuação do Estado e da iniciativa privada. O Governo pode estabelecer os planos e objetivos, otimizar sua estrutura, transformando autarquias já criadas por lei em agências reguladoras, buscando controlar e monitorar os serviços repassados. Nesse formato, a administração pública consegue se aproximar de forma prática ao dinamismo do mercado, buscando prestar serviços públicos de maior qualidade aos cidadãos, considerado aqui como consumidores, e focando mais fortemente nos fins ao invés dos meios, ou seja, nos resultados \cite{barcelos2013}.

O programa é visto pelo governo como uma política pública fundamentada em um modelo de gestão específico que tem como principais características o fato de ser essencialmente público - orientado ao cidadão e respeitando os princípios constitucionais da impessoalidade, da legalidade, da moralidade, da publicidade e da eficiência-, de ser contemporâneo - alinhado ao estado-da-arte da gestão -, de estar voltado para a disposição de resultados para a sociedade - com impactos na melhoria da qualidade de vida e na geração do bem comum - e de ser federativo - com aplicação a toda a administração pública, em todos os poderes e esferas do governo.

Nos últimos anos, o Gespública utilizou-se de uma estratégia de sucesso alicerçada no trabalho voluntário de representantes de instituições públicas, desenvolvendo e divulgando conceitos e soluções para gestão, implantando e mobilizando núcleos regionais e setoriais nas unidades da federação e realizando avaliações do nível de gestão das instituições, seja por mio de auto-avaliações, seja nos ciclos anuais do Prêmio Nacional da Gestão Pública.

O grande propósito da criação desse programa foi o de desenvolver o modelo de excelência em gestão pública. Para isso, ele está baseado em 13 fundamentos:

\begin{enumerate}

\item Pensamento sistêmico - organização como um sistema aberto: entre a organização e o ambiente externo existe uma relação de dependência;
\item Aprendizado organizacional - aprender com a experiência, troca de informações, treinamento e desenvolvimento contínuos;
\item Cultura da inovação - empreendedorismo: buscar novas e melhores formas de prestar serviços públicos;
\item Liderança e constância de propósitos;
\item Orientação por processos e informações;
\item Visão de futuro;
\item Geração de valor;
\item Comprometimento com as pessoas;
\item Foco no cidadão e na sociedade;
\item Desenvolvimento de parcerias;
\item Responsabilidade social;
\item Gestão participativa;
\item Controle social.  

\end{enumerate}

Em suma, a ideia do Gespública é promover uma mudança cultural no relacionamento entre o conjunto de instituições brasileiras. Esse é um desafio altamente complexo e de tal importância para a sociedade brasileira que não pode se restringir a poucos, por isso, a importância de se tratar sobre o assunto. A mudança cultural não é apenas em nível institucional, mas quando fala-se em controle social, está-se falando de uma gestão participativa, onde, principalmente, a população pode e deve exercer o controle externo sobre a atuação dos serviços públicos.


